package com.geek.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Rupesh kumar on 10/07/18.
 */

public class ItemResponse {

    @SerializedName("results")
    private List<Item> itemList;

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }
}
