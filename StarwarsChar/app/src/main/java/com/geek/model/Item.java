package com.geek.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Rupesh kumar on 10/07/18.
 */

public class Item implements Serializable {


    public Item(){

    }

    @SerializedName("edited")
    private String edited;

    @SerializedName("starships")
    private String[] starships;

    @SerializedName("species")
    private String[] species;

    @SerializedName("skin_color")
    private String skin_color;

    @SerializedName("eye_color")
    private String eye_color;

    @SerializedName("films")
    private String[] films;

    @SerializedName("birth_year")
    private String birth_year;

    @SerializedName("url")
    private String url;

    @SerializedName("mass")
    private String mass;

    @SerializedName("height")
    private String height;

    @SerializedName("created")
    private String created;

    @SerializedName("hair_color")
    private String hair_color;

    @SerializedName("name")
    private String name;

    @SerializedName("gender")
    private String gender;

    @SerializedName("homeworld")
    private String homeworld;

    @SerializedName("vehicles")
    private String[] vehicles;

    public String getEdited ()
    {
        return edited;
    }

    public void setEdited (String edited)
    {
        this.edited = edited;
    }

    public String[] getStarships ()
    {
        return starships;
    }

    public void setStarships (String[] starships)
    {
        this.starships = starships;
    }

    public String[] getSpecies ()
    {
        return species;
    }

    public void setSpecies (String[] species)
    {
        this.species = species;
    }

    public String getSkin_color ()
    {
        return skin_color;
    }

    public void setSkin_color (String skin_color)
    {
        this.skin_color = skin_color;
    }

    public String getEye_color ()
    {
        return eye_color;
    }

    public void setEye_color (String eye_color)
    {
        this.eye_color = eye_color;
    }

    public String[] getFilms ()
    {
        return films;
    }

    public void setFilms (String[] films)
    {
        this.films = films;
    }

    public String getBirth_year ()
    {
        return birth_year;
    }

    public void setBirth_year (String birth_year)
    {
        this.birth_year = birth_year;
    }

    public String getUrl ()
    {
        return url;
    }

    public void setUrl (String url)
    {
        this.url = url;
    }

    public String getMass ()
    {
        return mass;
    }

    public void setMass (String mass)
    {
        this.mass = mass;
    }

    public String getHeight ()
    {
        return height;
    }

    public void setHeight (String height)
    {
        this.height = height;
    }

    public String getCreated ()
    {
        return created;
    }

    public void setCreated (String created)
    {
        this.created = created;
    }

    public String getHair_color ()
    {
        return hair_color;
    }

    public void setHair_color (String hair_color)
    {
        this.hair_color = hair_color;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getHomeworld ()
    {
        return homeworld;
    }

    public void setHomeworld (String homeworld)
    {
        this.homeworld = homeworld;
    }

    public String[] getVehicles ()
    {
        return vehicles;
    }

    public void setVehicles (String[] vehicles)
    {
        this.vehicles = vehicles;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [edited = "+edited+", starships = "+starships+", species = "+species+", skin_color = "+skin_color+", eye_color = "+eye_color+", films = "+films+", birth_year = "+birth_year+", url = "+url+", mass = "+mass+", height = "+height+", created = "+created+", hair_color = "+hair_color+", name = "+name+", gender = "+gender+", homeworld = "+homeworld+", vehicles = "+vehicles+"]";
    }

}
