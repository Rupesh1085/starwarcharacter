package com.geek.viewmodel;

import android.content.Context;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.view.View;

import com.geek.R;
import com.geek.app.AppController;
import com.geek.model.Item;
import com.geek.model.ItemResponse;
import com.geek.network.ItemService;
import com.geek.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

import static com.geek.utils.Constant.ITEM_URL;

/**
 * Created by Rupesh kumar on 10/07/18.
 */

public class ItemViewModel extends Observable {

    public ObservableInt progressBar;
    public ObservableInt itemRecycler;
    public ObservableInt itemLabel;
    public ObservableField<String> messageLabel;

    private List<Item> itemList;
    private Context context;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public ItemViewModel(@NonNull Context context) {
        this.context = context;
        this.itemList = new ArrayList<>();
        progressBar = new ObservableInt(View.GONE);
        itemRecycler = new ObservableInt(View.GONE);
        itemLabel = new ObservableInt(View.VISIBLE);
        messageLabel = new ObservableField<>();
        initializeUIandSetUp();
    }

    public void onClickFabToLoad(View view) {
        initializeUIandSetUp();
    }

    public void initializeUIandSetUp(){
        if(AppUtils.checkInternet(context)){
            initializeViews();
            fetchItemList();
        }else{
            messageLabel = new ObservableField<>(context.getString(R.string.internet_connection));
        }
    }

    public void initializeViews() {
        itemLabel.set(View.GONE);
        itemRecycler.set(View.GONE);
        progressBar.set(View.VISIBLE);
    }

    private void fetchItemList() {

        AppController appController = AppController.create(context);
        ItemService itemService = appController.getItemService();
        Disposable disposable = itemService.fetchItems(ITEM_URL).
                subscribeOn(appController.subscribeScheduler()).
        observeOn(AndroidSchedulers.mainThread()).
        subscribe(new Consumer<ItemResponse>() {
            @Override
            public void accept(ItemResponse itemResponse) throws Exception {
                updateItemDataList(itemResponse.getItemList());
                progressBar.set(View.GONE);
                itemLabel.set(View.GONE);
                itemRecycler.set(View.VISIBLE);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {

                messageLabel.set(context.getString(R.string.error));
                progressBar.set(View.GONE);
                itemLabel.set(View.VISIBLE);
                itemRecycler.set(View.GONE);
            }
        });

        compositeDisposable.add(disposable);
    }


    private void updateItemDataList(List<Item> characters) {
        itemList.clear();
        itemList.addAll(characters);
        setChanged();
        notifyObservers(this);
    }

    public List<Item> getItemList() {
        return itemList;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }

}
