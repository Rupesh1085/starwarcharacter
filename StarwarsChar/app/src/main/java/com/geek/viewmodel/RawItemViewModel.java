package com.geek.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.view.View;

import com.geek.model.Item;
import com.geek.view.activity.CharacterDetailsActivity;

/**
 * Created by Rupesh kumar on 10/07/18.
 */

public class RawItemViewModel extends BaseObservable {
    private Item item;
    private Context context;

    public RawItemViewModel(Item _item, Context _context) {
        this.item = _item;
        this.context = _context;
    }

    public String getName() {
        return item.getName();
    }

    public String getMass() {
        return item.getMass();
    }

    public String getHeight() {
        return item.getHeight();
    }

    public String getCreatedTime() {
        return item.getCreated();
    }

    public void setItem(Item _item){
        this.item = _item;
        notifyChange();
    }

    public void onItemClick(View v){
        context.startActivity(CharacterDetailsActivity.fillDetail(v.getContext(), item));
    }

}
