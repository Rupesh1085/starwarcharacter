package com.geek.viewmodel;

import com.geek.model.Item;
import com.geek.utils.AppUtils;

import java.util.Observable;

/**
 * Created by Rupesh kumar on 10/07/18.
 */

public class ItemDetailViewModel extends Observable {
    private Item item;

    public ItemDetailViewModel(Item _item){
        this.item = _item;
    }

    public String getName(){
        return item.getName();
    }

    public String getHeight(){
        return item.getHeight();
    }

    public String getMass(){
        return item.getMass();
    }

    public String getCreated(){
        return AppUtils.getFormattedDate(item.getCreated());
    }
}
