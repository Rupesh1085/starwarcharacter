package com.geek.network;

import com.geek.model.ItemResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by Rupesh kumar on 10/07/18.
 */

public interface ItemService {

    @GET
    Observable<ItemResponse> fetchItems(@Url String url);

}
