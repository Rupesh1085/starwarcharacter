package com.geek.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.geek.R;
import com.geek.databinding.RawItemBinding;
import com.geek.model.Item;
import com.geek.viewmodel.RawItemViewModel;

import java.util.Collections;
import java.util.List;

/**
 * Created by Rupesh kumar on 10/07/18.
 */

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemAdapterViewHolder> {

    private List<Item> itemList;

    public ItemAdapter() {this.itemList = Collections.emptyList();}

    @Override
    public ItemAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RawItemBinding itemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.raw_item ,parent, false);
        return new ItemAdapterViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(ItemAdapterViewHolder holder, int position) {
        holder.bindItem(itemList.get(position));
    }

    @Override
    public int getItemCount() {
        return  itemList.size();
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
        notifyDataSetChanged();
    }

    public static class ItemAdapterViewHolder extends RecyclerView.ViewHolder {

        RawItemBinding mItemBinding;

        public ItemAdapterViewHolder(RawItemBinding itemBinding) {
            super(itemBinding.itemPeople);
            this.mItemBinding = itemBinding;
        }

        void bindItem(Item item){
            if(mItemBinding.getRawItemViewModel() == null){
                mItemBinding.setRawItemViewModel(new RawItemViewModel(item, itemView.getContext()));
            }else {
                mItemBinding.getRawItemViewModel().setItem(item);
            }
        }
    }
}
