package com.geek.view.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.geek.R;
import com.geek.databinding.ActivityCharacterDetailsBinding;
import com.geek.model.Item;
import com.geek.viewmodel.ItemDetailViewModel;

import static android.content.Intent.EXTRA_INTENT;

/**
 * Created by Rupesh kumar on 10/07/18.
 */

public class CharacterDetailsActivity extends AppCompatActivity {

    ActivityCharacterDetailsBinding activityCharacterDetailsBinding;
    ItemDetailViewModel itemDetailViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityCharacterDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_character_details);
        getExtrasFromIntent();
    }

    public static Intent fillDetail(Context context, Item item) {
        Intent intent = new Intent(context, CharacterDetailsActivity.class);
        intent.putExtra(EXTRA_INTENT, item);
        return intent;
    }

    private void getExtrasFromIntent(){
        Item item = (Item) getIntent().getSerializableExtra(EXTRA_INTENT);
        itemDetailViewModel = new ItemDetailViewModel(item);
        activityCharacterDetailsBinding.setItemDetailViewModel(itemDetailViewModel);
    }
}
