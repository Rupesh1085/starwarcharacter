package com.geek.view.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.geek.R;
import com.geek.databinding.ActivityCharacterListBinding;
import com.geek.view.adapter.ItemAdapter;
import com.geek.viewmodel.ItemViewModel;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by Rupesh kumar on 10/07/18.
 */

public class CharacterListActivity extends AppCompatActivity implements Observer {

    private ItemViewModel itemViewModel;
    private ActivityCharacterListBinding activityCharacterListBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initDataBinding();
        setUpListOfItemView(activityCharacterListBinding.listItem);
        setUpObserver(itemViewModel);

    }

    private void initDataBinding() {
        activityCharacterListBinding = DataBindingUtil.setContentView(this, R.layout.activity_character_list);
        itemViewModel = new ItemViewModel(this);
        activityCharacterListBinding.setItemViewModel(itemViewModel);
    }

    // set up the list of item with recycler view
    private void setUpListOfItemView(RecyclerView listUser) {
        ItemAdapter itemAdapter = new ItemAdapter();
        listUser.setAdapter(itemAdapter);
        listUser.setLayoutManager(new LinearLayoutManager(this));
    }

    public void setUpObserver(Observable observable) {
        observable.addObserver(CharacterListActivity.this);
    }

    @Override
    public void update(Observable observable, Object o) {
        if(o instanceof ItemViewModel) {
            ItemAdapter itemAdapter = (ItemAdapter) activityCharacterListBinding.listItem.getAdapter();
            ItemViewModel itemViewModel = (ItemViewModel) o;
            itemAdapter.setItemList(itemViewModel.getItemList());
            activityCharacterListBinding.listItem.scheduleLayoutAnimation();
        }
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        itemViewModel.reset();
    }
}
