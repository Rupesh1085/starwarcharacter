package com.geek.app;

import android.app.Application;
import android.content.Context;

import com.geek.network.ApiFactory;
import com.geek.network.ItemService;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Rupesh kumar on 10/07/18.
 */

public class AppController extends Application {

    private ItemService itemService;
    private Scheduler scheduler;

    private static AppController get(Context context) {
        return (AppController) context.getApplicationContext();
    }

    public static AppController create(Context context) {
        return AppController.get(context);
    }

    public ItemService getItemService() {
        if (itemService == null) {
            itemService = ApiFactory.create();
        }
        return itemService;
    }

    public Scheduler subscribeScheduler() {
        if (scheduler == null) {
            scheduler = Schedulers.io();
        }

        return scheduler;
    }

    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }

    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }


}
